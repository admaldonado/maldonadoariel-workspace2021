package ejercicio_semana1;

public class Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	// TIPOS PRIMITIVOS
		int numEntero = 8;
		double numFlotante = 2.3;
		boolean booleano = true;
		char caracter = 'A';
		
		//transformar variable de tipo
		int casteo = (int) 3.4;
		
		System.out.println("numero "+ casteo);
	
	// OPERADORES LOGICOS
		// IF
		if(numEntero == numFlotante) {
			System.out.println("los numeros son iguales");
		} else if (numEntero > numFlotante) {
			System.out.println("numero entero mayo a flotante");
		} else {
			System.out.println("el entero es menor o igual al flotante");
		}
		
		/* igualdad: ==
		 * desigual o distinto: !=
		 * menor: < (alt+60 en ascii)
		 * mayor: > (alt+62 en ascii)
		 * menor o igual: <=
		 * mayor o igual: >=
		 */
		
		if(numEntero != numFlotante && numEntero >7) {
			System.out.println("el num entero es distinto al flotante y mayor a siete");
		}
		/* &: y
		 * |: o
		 * &&: y, no evalua la segunda parte de la condicion si la primera es falsa
		 * ||: o, no evalua la segunda parte de la condicion si la primera es verdadera
		 * ^(alt+94 en ascii): XOR
		 */
		
		// IF CORTO
		System.out.println(numEntero > numFlotante? "entero > flotante" : "entero <= flotante"); //valor de retorno si es verdadero va primero, si es falso retorna lo que esta despues de los dos puntos
		
		int resultado = numEntero !=0 ? 100/numEntero : 0;
		
	//switch: estructura de seleccion multiple
		
		switch (caracter) {
		case 'A':
			System.out.println("el valor de caracter es A");
			break;  //no olvidarse
		case 'B':
			System.out.println("el valor del caracter es B");
			break;
			default: throw new IllegalArgumentException("unexpected value: "+ caracter);
		}
	//BUCLES
	int [] numerosEnteros = {1, 3, 5, 2, 6, 8, 7};
	
	int i=0;
	while(i< numerosEnteros.length) {
		System.out.println(numerosEnteros[i]);
		i = i+1;  // o usar i ++
	}
	//do while evalua la repeticion del bucle luego de ejecutarla una vez
	int j = 6;
	do {numerosEnteros[j] = j;
	System.out.println("num en do while: "+numerosEnteros[j]);
	j--;}   //o j = j-1
	while (j >=0);
	
	//for
	
	for (int k = 0; k < numerosEnteros.length; k++) {
		System.out.println("en for: " + numerosEnteros[k]);
	}
	
	//operadores de asignacion compuestos
	
	i+=7;  // i = i+
	i-=4;
	i*=5;
	i/=3;
	i%=9; //resto o igual
	
	++i;  //incrementa el valor y lo utiliza
	i++;  //utiliza el valor y luego incrementa
	
	// Comentario simple
		
	/* comentario de 
	 * multiples lineas
	 */
		
	/** comentario de javadoc
	 * 
	 */
		
		
	}

}
